import {
  SIGN_OUT_SUCCESS
} from 'src/core/auth';

import {
  CREATE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_SUCCESS,
  UPDATE_PRODUCT_SUCCESS,
  PRODUCT_LISTENERS_REGISTERED,
  PRODUCT_LOADING_START,
  PRODUCT_LOADING_END
} from './action-types';


export const initialState = {
  deleted: null,
  list: [],
  listening: false,
  loading: true,
  previous: []
};


export function productsReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_PRODUCT_SUCCESS:
      console.log('CREATE_PRODUCT_SUCCESS')
      return Object.assign({}, state, {
        loading: false,
        deleted: null,
        list: (state.deleted && state.deleted.key === action.payload.key) ?
              [ ...state.previous ] :
              [ action.payload, ...state.list ],
        previous: []
      });

    case DELETE_PRODUCT_SUCCESS:
      console.log('DELETE_PRODUCT_SUCCESS')
      return Object.assign({}, state, {
        loading: false,
        deleted: action.payload,
        list: state.list.filter(product => {
          return product.key !== action.payload.key;
        }),
        previous: [ ...state.list ]
      });

    case UPDATE_PRODUCT_SUCCESS:
      console.log('UPDATE_PRODUCT_SUCCESS');
      return Object.assign({}, state, {
        deleted: null,
        loading: false,
        list: state.list.map(product => {
          return product.key === action.payload.key ? action.payload : product;
        }),
        previous: []
      });

    case SIGN_OUT_SUCCESS:
      return Object.assign({}, state, {
        deleted: null,
        list: [],
        loading: false,
        listning: false,
        previous: []
      });

    case PRODUCT_LISTENERS_REGISTERED:
      return Object.assign({}, state, {listening: true});

    case PRODUCT_LOADING_START:
      return Object.assign({}, state, {loading: true});

    case PRODUCT_LOADING_END:
      return Object.assign({}, state, {loading: false});

    default:
      return state;
  }
}
