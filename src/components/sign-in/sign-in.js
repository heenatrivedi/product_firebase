import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { authActions } from 'src/core/auth';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';


const btnStyle = {
  width: '100%'
};

export function SignIn({/* signInWithGithub,, signInWithTwitter */signInWithGoogle }) {
  return (
    <div>
      <AppBar
          title={'Home'}
          iconElementLeft={<div></div>}
          iconElementRight={<FlatButton label="Login" primary={true} onClick={signInWithGoogle} />}
      />
      <div className="g-row sign-in">
        <div className="g-col">
          <h1 className="sign-in__heading">Sign in</h1>
          <RaisedButton label="Google" primary={true} style={btnStyle} onClick={signInWithGoogle} />
        </div>
      </div>
    </div>
  );
}

SignIn.propTypes = {
  signInWithGithub: PropTypes.func.isRequired,
  signInWithGoogle: PropTypes.func.isRequired,
  signInWithTwitter: PropTypes.func.isRequired
};


export default connect(null, authActions)(SignIn);
