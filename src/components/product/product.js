import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { authActions } from 'src/core/auth';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import {grey400, darkBlack} from 'material-ui/styles/colors';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import { notificationActions } from 'src/core/notification';
import { productsActions } from 'src/core/product';
import MenuItem from 'material-ui/MenuItem';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import CircularProgress from 'material-ui/CircularProgress';


const Fstyle = {
  position: 'fixed',
  bottom: 24,
  right: 24
};

export class Product extends Component {
  static propTypes = {
    deleteProduct: PropTypes.func.isRequired,
    registerListeners: PropTypes.func.isRequired,
    products: PropTypes.array.isRequired,
    listening: React.PropTypes.bool.isRequired,
    loading: React.PropTypes.bool.isRequired
  };
  componentWillMount() {
    if (!this.props.listening) this.props.registerListeners();
  }
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };
  constructor(props, context) {
    super(props, context);
    this.state = {dproduct: '', open: false};
    this.goToAddProduct = this.goToAddProduct.bind(this);
    this.goToProduct = this.goToProduct.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.confirmDelete = this.confirmDelete.bind(this);
    this.deleteListProduct = this.deleteListProduct.bind(this);
  }
  goToAddProduct() {
    this.context.router.push('/product/add');
  }
  goToProduct(item) {
    this.context.router.push('product/' + item.key + '/edit');
  }
  extraInfo(e) {
    e.stopPropagation();
  }
  handleClose() {
    this.setState({open: false, dproduct: ''});
  }
  confirmDelete() {
    this.props.deleteProduct(this.state.dproduct);
    this.handleClose();
  }
  deleteListProduct(dproduct) {
    this.setState({open: true, dproduct: dproduct});
  }
  componentWillReceiveProps(){
    console.log(this.props.listening)
    console.log(this.props.loading)
  }
  renderItem() {
    const {
      products
    } = this.props;

    var self = this;
    const iconButtonElement = (
      <IconButton
        onClick={this.extraInfo}
        touch={true}
        tooltip="more"
        tooltipPosition="bottom-left">
        <MoreVertIcon color={grey400} />
      </IconButton>
    );
    var productList = products.map(function(item, i) {
      let boundItemClick = self.goToProduct.bind(self, item);
      let rightIconMenu = (
        <IconMenu iconButtonElement={iconButtonElement} onClick={self.deleteListProduct.bind(self, item)}>
          <MenuItem>Delete</MenuItem>
        </IconMenu>
      );
      return (
        <div key={i}>
          <ListItem
            innerDivStyle={{padding: '16px 57px 16px 80px'}}
            key={i} onClick={boundItemClick}
            leftAvatar={<Avatar src={item.image ? item.image : 'images/ok-128.jpg'} style={{borderRadius: 0, height: 52, width: 52}} className="productlistimage" />}
            rightIconButton={rightIconMenu}
            primaryText={item.name}
            secondaryText={
              <p >
                <span style={{color: darkBlack}}>{item.sku}</span><br />
                {item.supplier}
              </p>
            }
            secondaryTextLines={2}
          />
            <Divider inset={true} />
          </div>
        );
    });
    return productList;
  }
  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Delete"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.confirmDelete}
      />
    ];
    return (
      <div>
       <AppBar
          title={'Products'}
          iconElementLeft={<div></div>}
       />
        <Dialog
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}>
          Delete this product?
        </Dialog>
        <div className="g-row ">
        {this.props.loading ? <CircularProgress className="loader" /> :
        <div className="g-col">
          <List>
            {this.renderItem()}
            </List>
          </div>
        }
          <FloatingActionButton style={Fstyle} onMouseUp={this.goToAddProduct} >
            <ContentAdd />
          </FloatingActionButton>

        </div>
       </div>
    );
  }
}



export default connect(state => ({
  notification: state.notification,
  listening: state.products.listening,
  loading: state.products.loading,
  products: state.products.list
}), Object.assign({}, authActions, productsActions, notificationActions))(Product);
