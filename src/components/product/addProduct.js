import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { authActions } from 'src/core/auth';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import { notificationActions } from 'src/core/notification';
import { productsActions } from 'src/core/product';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import IconButton from 'material-ui/IconButton';

const textStyle = {
  width: '100%'
};

export class AddProduct extends Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  static propTypes = {
    createProduct: PropTypes.func.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {error: '', product: {name: '', supplier: '', sku: '', upc: '', image: '/images/ok-128.jpg'}, file: ''};
    this.onChange = this.onChange.bind(this);
    this.addProduct = this.addProduct.bind(this);
    this.backToProduct = this.backToProduct.bind(this);
    this.changeFile = this.changeFile.bind(this);
  }
  onChange(field, event) {
    var product = this.state.product;
    product[field] = event.target.value;
    this.setState({product: product});

  }
  backToProduct() {
    this.context.router.push('/products');
  }
  changeFile(e) {
    var self = this;
    var file = e.target.files[0];
    var product = self.state.product;
    var reader = new FileReader();
    reader.onload = function(e) {
      var image = e.target.result;
      product.image = image;
      self.setState({file: file, product: product});
    };
    reader.readAsDataURL(file);
  }
  addProduct() {
    if (this.state.file === '' || this.state.file === undefined) {
      this.setState({error: 'please upload product image'});
    }
    else {
      this.props.createProduct(this.state.product, this.state.file, this.backToProduct());
    }
  }

  render() {
    return (
      <div>
        <AppBar
          title={'Add Product'}
          iconElementLeft={<IconButton onClick={this.backToProduct}><NavigationClose /></IconButton>}
          iconElementRight={<FlatButton label="Save" primary={true} onClick={this.addProduct} />}
        />
        <div className="g-row AddProduct ">
          <div className="g-col">
            <div className="imageupload">
              <input type="file" onChange={this.changeFile} />
              <Avatar src={this.state.product.image} style={{borderRadius: 0, height: 100, width: 100, backgroundSize: 100}} onClick={this.handleImageclick} />
            </div>
            {this.state.error === '' ? null : <div className="error">{this.state.error}</div>}
            <TextField
              hintText="Product Name"
              floatingLabelText="Product Name"
              style={textStyle}
              onChange={this.onChange.bind(null, 'name')}
              value={this.state.product.name}

            />
            <TextField
              hintText="Supplier"
              floatingLabelText="Supplier"
              style={textStyle}
              onChange={this.onChange.bind(null, 'supplier')}
              value={this.state.product.supplier}

            />
            <TextField
              hintText="UPC"
              floatingLabelText="UPC"
              style={textStyle}
              onChange={this.onChange.bind(null, 'upc')}
              value={this.state.product.upc}

            />
            <TextField
              hintText="SKU"
              floatingLabelText="SKU"
              style={textStyle}
              onChange={this.onChange.bind(null, 'sku')}
              value={this.state.product.sku}

            />
          </div>
        </div>
      </div>
    );
  }
}
export default connect(state => ({
  notification: state.notification,
  tasks: state.tasks.list
}), Object.assign({}, authActions, productsActions, notificationActions))(AddProduct);

