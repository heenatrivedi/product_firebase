import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { authActions } from 'src/core/auth';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import { productsActions } from 'src/core/product';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import IconButton from 'material-ui/IconButton';
import Dialog from 'material-ui/Dialog';


const textStyle = {
  width: '100%'
};

export class EditProduct extends Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  static propTypes = {
    deleteProduct: PropTypes.func.isRequired,
    registerListeners: PropTypes.func.isRequired,
    products: PropTypes.array.isRequired,
    getProductDetail: PropTypes.func.isRequired,
    updateProduct: PropTypes.func.isRequired,
    listening: React.PropTypes.bool.isRequired
  };
  componentWillMount() {
    if (!this.props.listening) this.props.registerListeners();
  }
  constructor(props) {
    super(props);
    var product = {name: '', sku: '', upc: '', supplier: '', image: '', image_name: ''};
    this.state = {product: product, open: false, file: ''};
    this.changeFile = this.changeFile.bind(this);
    this.backToProduct = this.backToProduct.bind(this);
    this.onChange = this.onChange.bind(this);
    this.updateProduct = this.updateProduct.bind(this);
    this.deleteProduct = this.deleteProduct.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.confirmDelete = this.confirmDelete.bind(this);
    this.bindProduct = this.bindProduct.bind(this);
  }
  componentDidMount() {
    this.bindProduct();
  }
  bindProduct() {
    var self = this;
    var product = '';
    if (this.props.products.length > 0) {
      this.props.products.forEach(function(prod) {
        if (prod.key === self.props.routeParams.product_id) {
          self.setState({product: prod});
          product = prod;
        }
      });
    }
    this.setState({product: product});
  }
  backToProduct() {
    this.context.router.push('/products');
  }
  onChange(field, event) {
    var product = this.state.product;
    product[field] = event.target.value;
    this.setState({product: product});

  }
  updateProduct() {
    this.props.updateProduct(this.state.product, this.state.file, this.backToProduct());
  }
  handleOpen() {
    this.setState({open: true});
  }

  handleClose() {
    this.setState({open: false});
  }
  confirmDelete() {
    this.props.deleteProduct(this.state.product);
    this.handleClose();
    this.backToProduct();
  }
  deleteProduct() {
    this.handleOpen();
  }
  changeFile(e) {
    var self = this;
    var file = e.target.files[0];
    var product = self.state.product;
    var reader = new FileReader();
    reader.onload = function(e) {
      var image = e.target.result;
      product.image = image;
      self.setState({file: file, product: product});
    };
    reader.readAsDataURL(file);
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Delete"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.confirmDelete}
      />
    ];
    return (
      <div>
        <AppBar
          title={'Edit Product'}
          iconElementLeft={<IconButton onClick={this.backToProduct}><NavigationClose /></IconButton>}
          iconElementRight={<FlatButton label="Save" primary={true} onClick={this.updateProduct} />}
        />

        <Dialog
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}>
          Delete this product?
        </Dialog>

        <div className="g-row AddProduct ">
          <div className="g-col">
            <div className="imageupload">
              <input type="file" onChange={this.changeFile} />
              <Avatar src={this.state.product.image} style={{borderRadius: 0, height: 100, width: 100, backgroundSize: 100}} onClick={this.handleImageclick} />
            </div>
            <TextField
              hintText="Product Name"
              floatingLabelText="Product Name"
              style={textStyle}
                onChange={this.onChange.bind(null, 'name')}
              value={this.state.product.name}
            />
            <TextField
              hintText="Supplier"
              floatingLabelText="Supplier"
              style={textStyle}
                onChange={this.onChange.bind(null, 'supplier')}
              value={this.state.product.supplier}
            />
            <TextField
              hintText="UPC"
              floatingLabelText="UPC"
              style={textStyle}
                onChange={this.onChange.bind(null, 'upc')}
              value={this.state.product.upc}
            />
            <TextField
              hintText="SKU"
              floatingLabelText="SKU"
              style={textStyle}
                onChange={this.onChange.bind(null, 'sku')}
              value={this.state.product.sku}
            />
          </div>
          <div className="footerAction">
            <FlatButton label="Delete" onClick={this.deleteProduct} />
          </div>
        </div>
      </div>
    );
  }
}
export default connect(state => ({
  notification: state.notification,
  listening: state.products.listening,
  products: state.products.list
}), Object.assign({}, authActions, productsActions))(EditProduct);

